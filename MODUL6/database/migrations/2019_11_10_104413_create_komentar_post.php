<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKomentarPost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('komentar_post', function (Blueprint $table) {
                $table->Increments('id')->unsigned();
                $table->integer('post_id')->unsigned();
                $table->foreign('post_id')->references('id')->on('post')->onDelete('cascade')->onUpdate('cascade');
                $table->integer('users_id')->unsigned();
                $table->foreign('users_id')->references('id')->on('users');
                $table->text('comment');
                $table->timestamps();
                });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('komentar_post');
    }
}
