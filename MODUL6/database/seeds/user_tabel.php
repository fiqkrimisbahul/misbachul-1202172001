<?php

use Illuminate\Database\Seeder;

class user_tabel extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'=> Str::random(10),
            'email'=> Str::random(10),
            'password'=> bcrypt('password'),
        ]);
    }
}
