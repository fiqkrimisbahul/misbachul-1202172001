<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    
        $this->middleware('auth');
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $posts= DB::table('post')->join('users','post.user_id','=','users.id')->select('post.*','users.name')->get();
        return view('home',[
            'post'=>$posts
        ]);
    }
    public function profile(){
        $users = DB::table('users')->select('users.*')->where('id',(Auth::user()->id))->first();
        $posts = DB::table('post')->join('users','post.user_id','=','users.id')->select('post.*','users.*')->get();
    
       return view('profile',['user'=>$users],['post'=>$posts]);
       
       }
}
