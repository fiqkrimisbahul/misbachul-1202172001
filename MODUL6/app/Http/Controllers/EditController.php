<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Auth;

class editController extends Controller
{
    public function edit()
    {
        
	$users = DB::table('users')->select('users.*')->where('id',(Auth::user()->id))->first();

	return view('edit');
	}
}
