<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Auth;

class editController extends Controller
{
	public function update(Request $request)
	{
	$users = DB::table('users')
	->select('users.*')
	->where('id',(Auth::user()->id))
    ->update(['name' => $request['name'],'description' => $request['description']]);
    
	return view('home');
}
}